# Mobile turtle care

### Installation
Install the dependencies and devDependencies.

For both environment
```sh
$ composer install
$ cp .env.example .env 
$ nano .env // set all credentials(ex: database, onesignal credentials)
$ php artisan migrate
```

For development environments...

```sh
$ npm install
$ npm run dev
```
For production environments...

```sh
$ npm install --production
$ npm run prod
```
create supervisor

run command after code changes

```sh
sudo supervisorctl reread && sudo supervisorctl update && sudo supervisorctl restart [queue-worker name] e.g. [turtleapp]
```

Extra commands

```sh
$ php artisan db:seed
$ php artisan storage:link
```

