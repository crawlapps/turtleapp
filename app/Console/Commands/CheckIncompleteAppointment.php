<?php

namespace App\Console\Commands;

use App\Models\Appointment;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;

class CheckIncompleteAppointment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'incomplete:appointment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try{
            logger('============== START:: CheckIncompleteAppointment ==============');

            $appointments = Appointment::where('appointment_time', '<=', Carbon::now())->get();

            if( count($appointments) > 0 ){
                $ids = $appointments->pluck('id');
                Appointment::whereIn('id', $ids)->update(['status' => 'in_completed']);
            }

            logger('============== END:: CheckIncompleteAppointment ==============');
        }catch ( \Exception $e ){
            logger('============== ERROR:: CheckIncompleteAppointment ==============');
            logger(json_encode($e));
        }
    }
}
