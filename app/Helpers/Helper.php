<?php

use App\Models\User;
use App\Models\UserAddress;
use Ichtrojan\Otp\Otp;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\Client as OClient;
if (!function_exists('sendMailH')) {
    /**
     * @param $data
     * @param $email
     * @param $format
     * @param $subject
     * @return mixed
     */
    function sendMailH($data, $email, $format, $subject)
    {
        try {
            $data = array('data' => $data);

            Mail::send($format, $data, function ($message) use ($email, $subject) {
                $message->to($email);
                $message->subject($subject);
                $message->getHeaders()->addTextHeader('Content-Type', 'text/html');
            });
            return 'success';
        }catch ( \Exception $e ){
            return $e->getMessage();
        }
    }
}

if (!function_exists('getTokenAndRefreshTokenH')) {

    /**
     * @param $email
     * @param $password
     * @param  string  $provider
     * @return mixed
     * token generate
     * @throws Exception
     */
    function getTokenAndRefreshTokenH($email, $password, $provider = 'users')
    {
        $oClient = OClient::where('password_client', 1)->first();
        $data = [
            'grant_type' => 'password',
            'client_id' => $oClient->id,
            'client_secret' => $oClient->secret,
            'username' => $email,
            'password' => $password,
            'scope' => '*',
            'provider' => $provider
        ];

        $response = \Illuminate\Http\Request::create('/oauth/token', 'POST', $data);
        $response = app()->handle($response);

        // Get the data from the response
        $data = json_decode($response->getContent());
        return $data;
    }
}

function generateOTP($email, $digits, $expire){
    $newsletter = app(Otp::class);
    return $newsletter->generate($email, $digits, $expire);
//    return rand(1258, 5896);
}
function validateOTP($email, $otp){
    $newsletter = app(Otp::class);
    return $newsletter->validate($email, $otp);
//    return rand(1258, 5896);
}
function generateReferralCode(){
    $upperAlpha = range('A', 'Z');
    $lowerAlpha =  range('a', 'z');
    $digits = range('0', '9');

    shuffle($lowerAlpha);
    shuffle($upperAlpha);
    shuffle($digits);

    $alphas = array_merge($upperAlpha, $lowerAlpha);
    $alphaDigits = array_merge($alphas, $digits);

    shuffle($alphaDigits);

    return implode('', array_slice($alphaDigits,5,10));
}

function getReferralCode(){
    $code = generateReferralCode();
    $is_existCode = User::where('referral_code', $code)->count();
    if( $is_existCode > 0 ){
        getReferralCode();
    }else{
        return $code;
    }
}

if (!function_exists('getUserIdsByAppointment')) {

    /**
     * @param $appointment
     * @param $edit_user_id
     * @return array
     */
    function getUserIdsByAppointment($appointment, $edit_user_id)
    {
        try{
            $admin = User::where('user_type', 'admin')->get()->pluck('id')->toArray();

            $edit_user_id = ($edit_user_id == '') ? $admin[0] : $edit_user_id;

            $users = explode(',', $appointment->user_ids);
            $edited_user = User::select('firstname', 'lastname')->where('id', $edit_user_id)->first();
            $ids = array_merge($admin, $users);
            array_push($ids, $appointment->user_id);

            $is_edited_user = array_search($edit_user_id, $ids);

            if( $is_edited_user || $is_edited_user == 0 ){
                array_splice($ids, $is_edited_user, 1);
            }

//            array_push($ids, 100);
            array_filter($ids);
            $res['ids'] = $ids;
            $res['is_admin'] = in_array($edit_user_id, $admin);
            $res['user'] = ($edited_user) ? $edited_user->toArray() : [];
            return $res;
        }catch ( \Exception $e ){
            logger("========== ERROR::  ==========");
            logger($e->getMessage());
        }
    }
}

if (!function_exists('createNotificationH')) {

    /**
     * @param $appointment
     * @param $edit_user_id
     * @return array
     */
    function createNotificationH($appointment, $edit_user_id, $action)
    {
        try{
            $sendUsersData = getUserIdsByAppointment($appointment, $edit_user_id);

            if( !empty($sendUsersData['user'])){
                $message = 'Appointment is '.$action.' by ' . $sendUsersData['user']['firstname'] . ' ' . $sendUsersData['user']['lastname'];

                $message .= ( $sendUsersData['is_admin'] ) ? ' (Admin)' : '';
            }else{
                $message = 'Appointment is '.$action;
                $message .= ( $sendUsersData['is_admin'] ) ? ' (Admin)' : '';
            }
            $data['appointment_id'] = $appointment->id;
            $data['action'] = $action;
            sendNotification($data, $sendUsersData['ids'], $message);
            return;
        }catch ( \Exception $e ){
            logger("========== ERROR::  ==========");
            logger($e->getMessage());
        }
    }
}

if (!function_exists('sendNotification')) {
    /**
     * @param $data
     * @param $users
     * @param $message
     * @return bool|string
     */
    function sendNotification($data, $users, $message)
    {
        $rest_api_key = env('ONESIGNAL_REST_API_KEY');
        $app_id = env('ONESIGNAL_APP_ID');

        $content = array(
            "en" => $message,
        );

        $i = 0;

        $users = array_filter($users);
        logger(json_encode($users));
        foreach ($users as $key => $val) {
            if( $val != '' ){
                $filters[] = (array("field" => "tag", "key" => "user_id", "relation" => "=", "value" => $val));

                if( count( $users ) > 1 ){
                    if (count($users) != ($key + 1)) {
                        $filters[] = ["operator" => "OR"];
                    }
                }
            }

            $i++;
        }


        $fields = array(
            'filters' => $filters,
            'app_id' => $app_id,
            'data' => $data,
            'contents' => $content,
        );

        $fields = json_encode($fields);

        logger($fields);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            'Authorization: Basic '.$rest_api_key,
            'cache-control: no-cache',
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($ch);
        curl_close($ch);

        \Log::info($response);
//        dump($response);
        return $response;
    }
}

if (!function_exists('getAddressByUser')) {

    /**
     * @param $user_id
     * @return mixed
     */
    function getAddressByUser($user_id, $type){

        if( $type == 'user' ){
            $address = UserAddress::select('id', 'address', 'latitude', 'longitude')->where('user_id', $user_id)->first();
        }else{
            $address = UserAddress::select('id', 'address', 'latitude', 'longitude')->where('id', $user_id)->first();
        }

        if( $address ){
            $address = $address->toArray();
            $address['address_id'] = $address['id'];
            unset($address['id']);
        }else{
            $address['address_id'] = '';
            $address['address'] = '';
            $address['latitude'] = '';
            $address['longitude'] = '';
        }
        return $address;
    }
}

