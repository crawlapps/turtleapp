<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Jobs\AppointmentNotificationJob;
use App\Models\Admin;
use App\Models\Appointment;
use App\Models\User;
use App\Models\Service;
use App\Models\Staff;
use App\Models\UserAddress;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Request;

class AppointmentController extends Controller
{
    public function login(Request $request){
        try{
            $inputs = $request::all();
            $rules = [
                'email' => 'required',
                'password' => 'required',
            ];
            $validator = Validator::make($request::all(), $rules);
            if ($validator->fails())
                return response()->json(['error' =>  $validator->messages() ], 422);

            $admin = Admin::where('email', $inputs['email'])->first();
            if( $admin ){
                if( !Hash::check( $inputs['password'], $admin->password ) ) {
                    return response()->json(['error' => ['message' => 'Incorrect password...please try again.' ] ], 422);
                }

                Auth::login($admin);
                $admin = $admin->toArray();
                unset($admin['password']);

                $token = (array)getTokenAndRefreshTokenH( $admin['email'], $inputs['password'], 'admin');

                $data = array_merge($admin, $token);
                return response()->json(['data' => $data], 200);
            }else
                return response()->json(['error' => ['message' => 'Incorrect email...please try again.' ] ], 422);


        }catch( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }

    public function getStaff(Request $request){
        try{
            $staff = Staff::select('id', 'name', 'type', 'specialist_in', 'license_id', 'license_type', 'licensed_states')->get()->toArray();

            return response()->json(['data' => $staff], 200);
        }catch( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }

    public function getAppointments(Request $request){
        try{
            $inputs = $request::all();
            $rules = [
                'status' => 'required',
            ];
            $validator = Validator::make($request::all(), $rules);
            if ($validator->fails()) {
                return response()->json(['error' =>  $validator->messages() ], 422);
            }

            // $appointments = ($inputs['status'] == 'begin') ? Appointment::where('status', $inputs['status'])->orderBy('created_at', 'desc')->get() : Appointment::where('status', '!=', 'begin')->orderBy('created_at', 'desc')->get();

              $appointments = Appointment::where('status', $inputs['status'])->orderBy('created_at', 'desc')->get();
             $res = [];
            if( count($appointments) > 0 ){
                $appointments = $appointments->toArray();
                   foreach ($appointments as $key => $value) {
                       $user_id = $value['user_id'];
                       $user = User::select('id AS user_id', 'firstname', 'lastname' , 'phone', 'email')->where('id', $user_id)->first()->toArray();
                       $service = Service::select('name')->where('id', $value['service_id'])->first();

                        $address = UserAddress::select('id', 'address', 'latitude', 'longitude')->where('id', $value['address_id'])->first();

                        if( $address ){
                            $address = $address->toArray();
                            $address['address_id'] = $address['id'];
                            unset($address['id']);
                        }else{
                            $address['address_id'] = '';
                            $address['address'] = '';
                            $address['latitude'] = '';
                            $address['longitude'] = '';
                        }

                       $value = array_merge($value, $user);
                       $value = array_merge($value, $address);

                       //get child patients
                       $value['user_ids'] = explode(',', $value['user_ids']);
                       $childPatients = [];
                       foreach ($value['user_ids'] as $childK => $childV) {
                           $user = User::select('id', 'firstname', 'lastname')->where('id', $childV)->first();
                           ($user) ? array_push($childPatients, $user) : '';

                       }
                       unset($value['user_ids']);
                       $value['child_patients'] = $childPatients;

                       // get child staff
                       $value['staff_id'] = explode(',', $value['staff_id']);
                       $staffs = [];
                       foreach ($value['staff_id'] as $childK => $childV) {
                           $staff = Staff::select('id', 'name', 'type')->where('id', $childV)->first();
                           ($staff) ? array_push($staffs, $staff) : '';

                       }
                       unset($value['staff_id']);
                       $value['staffs'] = $staffs;
                       $value['service_name'] = $service->name;
                       array_push($res, $value);
                   }
            }

            return response()->json(['data' => $res], 200);
        }catch( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }

    public function changeAppointmentStatus(Request $request){
        try{
            $inputs = $request::all();
            $rules = [
                'appointment_id' => 'required',
                'status' => 'required'
            ];

            if( @$inputs['status'] == 'confirmed'){
                $rules['staff_id'] = 'required';
            }

            $validator = Validator::make($request::all(), $rules);
            if ($validator->fails()) {
                return response()->json(['error' =>  $validator->messages() ], 422);
            }

            $appointment = Appointment::find($inputs['appointment_id']);
            if( @$inputs['status'] == 'confirmed' ){
                $staff_id = implode(',', json_decode($inputs['staff_id']));
                $appointment->update(['staff_id' => $staff_id, 'status' => $inputs['status']]);
            } else
                $appointment->update(['status' => $inputs['status']]);

            $appointment->save();

            if( $inputs['status'] == 'confirmed' || $inputs['status'] == 'cancelled' ){
                AppointmentNotificationJob::dispatch($appointment, '', $inputs['status']);
            }

            return response()->json(['data' => 'Appointment '. $inputs['status'] .' successfully...'], 200);
        }catch( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }
}
