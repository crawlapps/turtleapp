<?php

namespace App\Http\Controllers\Api\Staff;

use App\Http\Controllers\Controller;
use App\Models\Staff;
use Request;

class StaffController extends Controller
{
    public function index(){
        try{
            $staff = Staff::select('id', 'name', 'type')->get()->toArray();
            return response()->json(['data' => $staff], 200);
        }catch ( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }
}
