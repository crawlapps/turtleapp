<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Jobs\AppointmentNotificationJob;
use App\Models\Appointment;
use App\Models\Service;
use App\Models\Staff;
use App\Models\User;
use App\Models\UserAddress;
use Illuminate\Database\Eloquent\Model;
use Request;
use Illuminate\Support\Facades\Validator;

class AppointmentController extends Controller
{
    public function createAppointment(Request $request){
        try{
            $inputs = $request::all();

            $rules = [
                'user_id' => 'required',
                'service_id' => 'required',
                'address_id' => 'required',
                'description' => 'required',
                'appointment_time' => 'required',
            ];

            if( $inputs['address_id'] == '' ){
                unset($rules['address_id']);
                $rules['latitude'] = 'required';
                $rules['longitude'] = 'required';
                $rules['address'] = 'required';
            }
            $validator = Validator::make($request::all(), $rules);
            if ($validator->fails()) {
                return response()->json(['error' =>  $validator->messages() ], 422);
            }

            $action = 'send';
            if( @$inputs['appointment_id'] ){
                $appointment = Appointment::find($inputs['appointment_id']);
                if( !$appointment )
                    return response()->json(['error' => ['message' => 'Appointment not found...' ] ], 422);

                $action = 'edit';
            }else{
                $appointment = new Appointment;
            }
            if( $inputs['address_id'] == '' ){
                $is_exist_add = UserAddress::where('user_id', $inputs['user_id'])->where('address', 'LIKE', $inputs['address'])->first();

                $address = ($is_exist_add) ? $is_exist_add : new UserAddress;
                $address->user_id = $inputs['user_id'];
                $address->address = $inputs['address'];
                $address->latitude = $inputs['latitude'];
                $address->longitude = $inputs['longitude'];
                $address->is_main = 0;
                $address->save();

                $inputs['address_id'] = $address->id;
            }

            $appointment->user_id = $inputs['user_id'];
            $appointment->address_id = $inputs['address_id'];
            $appointment->service_id = $inputs['service_id'];
            $appointment->description = $inputs['description'];
            $appointment->appointment_time = date('Y-m-d H:i:s', strtotime($inputs['appointment_time']));
            $appointment->status = 'begin';

            $appointment->user_ids = implode(',', json_decode($inputs['user_ids']));
            $appointment->save();

            $a = (  $action == 'send' ) ? 'created' : 'updated';
            $data['message'] = 'Appointment '. $action .' successfully...';
            $data['id'] = $appointment->id;
            AppointmentNotificationJob::dispatch($appointment, $inputs['user_id'], $a);
            return response()->json(['data' => $data], 200);
        }catch ( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }

    public function getAppointments(Request $request){
        try{
            $inputs = $request::all();

            $rules = [
                'user_id' => 'required',
            ];

            $status = ( @$inputs['status'] ) ? $inputs['status'] : 'begin';
            $validator = Validator::make($request::all(), $rules);
            if ($validator->fails()) {
                return response()->json(['error' =>  $validator->messages() ], 422);
            }

            $asMainAppointments = Appointment::where('user_id', $inputs['user_id'])->where('status', $status)->orderBy('created_at', 'desc')->get();

//            $asAChildAppointments = Appointment::select('user_id AS parent_user_id')->where('user_ids', 'LIKE', '%,'.$inputs['user_id'].',%')->where('status', $status)->orderBy('created_at', 'desc')->get();


            $appointments = ( count($asMainAppointments) > 0 ) ? $asMainAppointments->toArray() : [];
//            $childAppointments = ( count($asAChildAppointments) > 0 ) ? $asAChildAppointments->toArray() : [];

//            $appointments = array_merge($mainAppointments, $childAppointments);
            $res = [];
            if( count($appointments) > 0 ){
                foreach ($appointments as $key => $value) {
                    $user_id = $value['user_id'];
                    $user = User::select('id AS user_id', 'firstname', 'lastname', 'phone', 'email' )->where('id', $user_id)->first()->toArray();
                    $service = Service::select('name', 'image')->where('id', $value['service_id'])->first();
                    $staff = Staff::select('name', 'type')->where('id', $value['staff_id'])->first();

                    $address = getAddressByUser($value['address_id'],'appoint');

                    $value = array_merge($value, $user);
                    $value = array_merge($value, $address);
                    $value['user_ids'] = explode(',', $value['user_ids']);

                    //get child patients
                    $childPatients = [];
                    foreach ($value['user_ids'] as $childK => $childV) {
                        $user = User::select('id', 'firstname', 'lastname')->where('id', $childV)->first();
                        ($user) ? array_push($childPatients, $user) : '';

                    }
                    unset($value['user_ids']);
                    $value['child_patients'] = $childPatients;

                    // get child staff
                    $value['staff_id'] = explode(',', $value['staff_id']);
                    $staffs = [];
                    foreach ($value['staff_id'] as $childK => $childV) {
                        $staff = Staff::select('id', 'name', 'type')->where('id', $childV)->first();
                        ($staff) ? array_push($staffs, $staff) : '';

                    }
                    unset($value['staff_id']);
                    $value['staffs'] = $staffs;

                    $value['service']['name'] = $service->name;
                    $value['service']['image'] = \Storage::disk('public')->url('images/static/'.$service->image);
                    array_push($res, $value);
                }
            }

            return response()->json(['data' => $res], 200);
        }catch ( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }

    public function cancelAppointment(Request $request){
        try{
            $inputs = $request::all();

            $rules = [
                'user_id' => 'required',
                'appointment_id' => 'required',
            ];
            $validator = Validator::make($request::all(), $rules);
            if ($validator->fails()) {
                return response()->json(['error' =>  $validator->messages() ], 422);
            }

            $appointment = Appointment::where('id', $inputs['appointment_id'])->where('user_id', $inputs['user_id'])->first();

            if($appointment){
                $appointment->update(['status' => 'cancelled']);
                $appointment->save();

                AppointmentNotificationJob::dispatch($appointment, $inputs['user_id'], 'cancelled');

                return response()->json(['data' => 'Appointment cancelled successfully...'], 200);
            }else{
                return response()->json(['data' => 'Appointment not found...'], 422);
            }
        }catch ( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }

    public function getHistory(Request $request){
        try{
            $rules = [
                'user_id' => 'required',
                'status' => 'required',
            ];

            $validator = Validator::make($request::all(), $rules);
            if ($validator->fails()) {
                return response()->json(['error' =>  $validator->messages() ], 422);
            }
            $inputs = $request::all();

            $status = json_decode($inputs['status']);
            $appointments = Appointment::where('user_id', $inputs['user_id'])->whereIn('status', $status)->get();

            $res = [];
            if( count($appointments) > 0 ){
                $appointments = $appointments->toArray();
                foreach ($appointments as $key => $value) {
                    $user_id = $value['user_id'];
                    $user = User::select('id AS user_id', 'firstname', 'lastname', 'phone', 'email' )->where('id', $user_id)->first()->toArray();
                    $service = Service::select('name', 'image')->where('id', $value['service_id'])->first();
                    $staff = Staff::select('name', 'type')->where('id', $value['staff_id'])->first();

                    $address = getAddressByUser($value['address_id'],'appoint');

                    $value = array_merge($value, $user);
                    $value = array_merge($value, $address);
                    $value['user_ids'] = explode(',', $value['user_ids']);

                    //get child patients
                    $childPatients = [];
                    foreach ($value['user_ids'] as $childK => $childV) {
                        $user = User::select('id', 'firstname', 'lastname')->where('id', $childV)->first();
                        ($user) ? array_push($childPatients, $user) : '';

                    }
                    unset($value['user_ids']);
                    $value['child_patients'] = $childPatients;

                    // get child staff
                    $value['staff_id'] = explode(',', $value['staff_id']);
                    $staffs = [];
                    foreach ($value['staff_id'] as $childK => $childV) {
                        $staff = Staff::select('id', 'name', 'type')->where('id', $childV)->first();
                        ($staff) ? array_push($staffs, $staff) : '';

                    }
                    unset($value['staff_id']);
                    $value['staffs'] = $staffs;

                    $value['service']['id'] = $value['service_id'];
                    $value['service']['name'] = $service->name;
                    $value['service']['image'] = \Storage::disk('public')->url('images/static/'.$service->image);
                    array_push($res, $value);
                }
            }

            return response()->json(['data' => $res], 200);
        }catch( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }

    public function getAppointmentsByStatus(Request $request){
        try{
            $rules = [
                'user_id' => 'required',
                'status' => 'required',
            ];

            $validator = Validator::make($request::all(), $rules);
            if ($validator->fails()) {
                return response()->json(['error' =>  $validator->messages() ], 422);
            }
            $inputs = $request::all();

            $status = json_decode($inputs['status']);
            $appointments = Appointment::where('user_id', $inputs['user_id'])->whereIn('status', $status)->get();

            $res = [];
            if( count($appointments) > 0 ){
                $appointments = $appointments->toArray();
                foreach ($appointments as $key => $value) {
                    $user_id = $value['user_id'];
                    $user = User::select('id AS user_id', 'firstname', 'lastname', 'phone', 'email' )->where('id', $user_id)->first()->toArray();
                    $service = Service::select('name', 'image')->where('id', $value['service_id'])->first();
                    $staff = Staff::select('name', 'type')->where('id', $value['staff_id'])->first();

                    $address = getAddressByUser($value['address_id'],'appoint');

                    $value = array_merge($value, $user);
                    $value = array_merge($value, $address);
                    $value['user_ids'] = explode(',', $value['user_ids']);

                    //get child patients
                    $childPatients = [];
                    foreach ($value['user_ids'] as $childK => $childV) {
                        $user = User::select('id', 'firstname', 'lastname')->where('id', $childV)->first();
                        ($user) ? array_push($childPatients, $user) : '';

                    }
                    unset($value['user_ids']);
                    $value['child_patients'] = $childPatients;

                    // get child staff
                    $value['staff_id'] = explode(',', $value['staff_id']);
                    $staffs = [];
                    foreach ($value['staff_id'] as $childK => $childV) {
                        $staff = Staff::select('id', 'name', 'type')->where('id', $childV)->first();
                        ($staff) ? array_push($staffs, $staff) : '';

                    }
                    unset($value['staff_id']);
                    $value['staffs'] = $staffs;

                    $value['service']['id'] = $value['service_id'];
                    $value['service']['name'] = $service->name;
                    $value['service']['image'] = \Storage::disk('public')->url('images/static/'.$service->image);
                    array_push($res, $value);
                }
            }

            return response()->json(['data' => $res], 200);
        }catch( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }
}
