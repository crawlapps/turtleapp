<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\LoginRequest;
use App\Models\User;
use App\Models\UserAddress;
use Ichtrojan\Otp\Otp;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\Client as OClient;
use Request;
use Illuminate\Support\Facades\Validator;

class LoginController extends Controller
{
    public function register(Request $request)
    {
        try {
            $inputs = $request::all();

            $rules = [
                'firstname' => 'required',
                'phone' => 'required',
            ];

            if( (bool)$inputs['is_secondary'] ){
                $rules['parent_relation'] = 'required';
                $rules['parent_id'] = 'required';
                $rules['email'] = 'required|unique:users';
            }else{
                $rules['erolled_in_medicadid'] = 'required|integer|min:0|max:1';
                $rules['use_insurance'] = 'required|integer|min:0|max:1';
                $rules['login_type'] = 'required';

                if( $inputs['login_type'] == 'custom' ){
                    $rules['birthdate'] = 'required';
                    $rules['gender'] = 'required|string|min:1|max:1|';
//                    $rules['email'] = 'required|unique:users';
                    $rules['password'] = 'required';
                    $rules['lastname'] = 'required';
                }else{
                    $rules['email'] = 'required|unique:users,email,' . $inputs['provider_id'] . ',provider_id';
                    $rules['provider_id'] = 'required';
                    unset($rules['phone']);
                }
            }

            $validator = Validator::make($request::all(), $rules);
            if ($validator->fails()) {
                return response()->json(['error' =>  $validator->messages() ], 422);
            }

            if( !(bool)$inputs['is_secondary'] ){
                if( @$inputs['birthdate'] ){
                    $inputs['birthdate'] = date('Y-m-d', strtotime($inputs['birthdate']));
                }


                $password = ( $inputs['login_type'] == 'custom') ? $inputs['password'] : $inputs['provider_id'];
                $inputs['password'] = base64_encode($password);
            }else{
                $inputs['refer_by'] = $inputs['parent_id'];
            }
//            $inputs['referral_code'] = getReferralCode();
            if( $inputs['login_type'] == 'custom' ){
                // user who is secondary and register with referral code
                if( @$inputs['referee_code'] || @$inputs['referee_code'] != '' ){
                    $user = User::where('email', $inputs['email'])->first();
                    if( !$user ){
                        return response()->json(['error' => ['message' => 'Email is incorrect'] ], 422);
                    }

                    $parentUser = User::find($user->refer_by);
                    if( $inputs['referee_code'] != $parentUser->referral_code ){
                        return response()->json(['error' => ['message' => 'Invalid referral code.please try again'] ], 422);
                    }

                    if( !(bool)$user->is_otp_verified ){
                        $updateData = $inputs;

                        unset($updateData['is_parent']);
                        unset($updateData['parent_id']);
                        unset($updateData['parent_relation']);
                        $updateData['id'] = $user->id;
                        $inputs['referral_code'] = getReferralCode();
                        $otp = generateOTP($user->email, 4, 2);

                        $user->update($updateData);
                        $user = User::find($user->id);
                        $user = $user->toArray();
                        $user['otp'] = $otp->token;
                    }else {
                        return response()->json(['error' => ['email' => ["The email has already been taken."] ] ], 422);
                    }
                }else{
                    //new user or secondary user register without referral code
                    $is_exist_email = User::where('email', $inputs['email'])->first();
                    if( $is_exist_email ){
                        // secondary user without referral code
                        if( !(bool)$is_exist_email->is_parent && !(bool)$is_exist_email->is_otp_verified){
                            // update secondary user with parent user
                            $updateData = $inputs;
                            $updateData['is_parent'] = 1;
                            $updateData['parent_id'] = 0;
                            $updateData['parent_relation'] = '';
                            $updateData['id'] = $is_exist_email->id;
                            $otp = generateOTP($inputs['email'], 4, 2);

                            $is_exist_email->update($updateData);
                            $user = User::find($is_exist_email->id);
                            $user = $user->toArray();
                            $user['otp'] = $otp->token;
                        }else{
                            // custom user register with email which already registered
                            return response()->json(['error' => ['email' => ["The email has already been taken."] ] ], 422);
                        }
                    }else{
                        // new custom user register

                        if( @$inputs['birthdate'] ){
                            $inputs['birthdate'] = date('Y-m-d', strtotime($inputs['birthdate']));
                        }
                        if( (bool)$inputs['is_secondary'] ){
                        }else{
                            $inputs['referral_code'] = getReferralCode();
                            $otp = generateOTP($inputs['email'], 4, 2);
                        }
                        $user = User::create($inputs);
                        $user = $user->toArray();

                        if(!(bool)$inputs['is_secondary'] ){
                            $user['otp'] = $otp->token;
                        }
                    }
                }
            }else{
                // user who login/register with social account
                $user = User::where('provider_id', $inputs['provider_id'])->first();
                if( !$user ){
                    // register social user
                    $same_email_count = User::where('email', $inputs['email'])->count();
                    if( $same_email_count > 0 ){
                        return response()->json(['error' => ['email' => ["The email has already been taken."] ] ], 422);
                    }
                    $inputs['is_parent'] = 1;
                    $inputs['is_secondary'] = 0;
                    $inputs['referral_code'] = getReferralCode();
                    $otp = generateOTP($inputs['email'], 4, 2);
                    $user = User::create($inputs);

                    if( @$inputs['avtar'] != '' ){
                        $path = $inputs['avtar'];
                        $filename = basename($path);

                        $newPath = storage_path().'/app/public/images/avtar/' . $user->id ;
                        if(!\File::isDirectory($newPath)){
                            \File::makeDirectory($newPath, 0777, true, true);
                        }
                        file_put_contents($newPath . '/' . $filename, file_get_contents($path));

                        $user->avtar = $filename;
                        $user->save();
                    }

                    $user = $user->toArray();
                    $user['otp'] = $otp->token;
                }else{
                    // login social user
                    if( !$user->is_otp_verified ){
                        $otp = generateOTP($inputs['email'], 4, 2);
//                        $user->otp = generateOTP();
//                        $user->save();
                        if( $otp->status ){
                            $user = $user->toArray();
                            $user['otp'] = $otp->token;
                            $res = sendMailH($user, $inputs['email'], 'MailView.registerOtp', 'Verify email for TurtleCare');
                            if( $res == 'success' ){
                                $data['id'] = $user['id'];
                                $data['otp'] = $user['otp'];
                                $data['message'] = 'not_verified';
                                return response()->json(['data' => $data], 403);
                            }else{
                                return response()->json(['error' => ['message' => $res ] ], 422);
                            }
                        }else{
                            return response()->json(['error' => ['message' => $otp->message ] ], 422);
                        }
                    }else{
                        $user->firstname = $inputs['firstname'];
                        $user->lastname = $inputs['lastname'];
                        $user->birthdate = ( @$inputs['birthdate'] ) ? date('Y-m-d', strtotime($inputs['birthdate'])) : $user->birthdate;
                        $user->gender = ( @$inputs['gender'] ) ? $inputs['gender'] : $user->gender;
                        $user->phone = ( @$inputs['phone'] ) ? $inputs['phone'] : $user->phone;

                        if( @$inputs['avtar'] != '' ){
                            $path = $inputs['avtar'];
                            $filename = basename($path);

                            $newPath = storage_path().'/app/public/images/avtar/' . $user->id ;
                            if(!\File::isDirectory($newPath)){
                                \File::makeDirectory($newPath, 0777, true, true);
                            }
                            $oldpath = storage_path().'/app/public/images/avtar/'. $user->id .'/'.$inputs['avtar'];
                            if(\File::exists($oldpath)){
                                unlink($oldpath);
                            }
                            file_put_contents($newPath . '/' . $filename, file_get_contents($path));

                            $user->avtar = $filename;
                        }
                        $user->save();

                        $user = $user->toArray();
                        $user['avtar'] = ( @$user['avtar'] != '' ) ? \Storage::disk('public')->url('images/avtar/'.$user['id'] .'/'.$user['avtar']) : '';

                        $token = (array)getTokenAndRefreshTokenH($user['email'], $password);

                        $address = UserAddress::select('id', 'address', 'latitude', 'longitude')->where('user_id', $user['id'])->first();

                        if( $address ){
                            $address = $address->toArray();
                            $address['address_id'] = $address['id'];
                            unset($address['id']);
                        }else{
                            $address['address_id'] = '';
                            $address['address'] = '';
                            $address['latitude'] = '';
                            $address['longitude'] = '';
                        }
                        $data = array_merge($user, $address);
                        $data = array_merge($data, $token);

                        return response()->json(['data' => $data], 200);
                    }
                }
            }
            $user = (!is_array($user)) ? $user->toArray() : $user;
            if( !(bool)$inputs['is_secondary'] ){
                // send otp for new social user or new registered parent user

                $res = sendMailH($user, $inputs['email'], 'MailView.registerOtp', 'Verify email for TurtleCare');

                if( $res == 'success' ){
                    $data['id'] = $user['id'];
                    $data['otp'] = $user['otp'];
                    return response()->json(['data' => $data], 200);
                }else{
                    return response()->json(['error' => ['message' => $res ] ], 422);
                }
                $data = $user;
            }else{
                $parentUser = User::find($user['parent_id']);
                if( $parentUser->referral_code == '' || $parentUser->referral_code == null ){
                    $parentUser->referral_code = getReferralCode();
                    $parentUser->save();
                }
                $user['referee_code'] = $parentUser->referral_code;
                $res = sendMailH($user, $inputs['email'], 'MailView.referralCode', 'Your referral code');
                $data = $user;
            }

            return response()->json(['data' => $data], 200);

        } catch(Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }

    public function verifyOtp(Request $request){
        try{
            $inputs = $request::all();
            $rules = [
                'user_id' => 'required',
                'otp' => 'required',
            ];
            $validator = Validator::make($request::all(), $rules);
            if ($validator->fails()) {
                return response()->json(['error' =>  $validator->messages() ], 422);
            }

            $user = User::where('id', $inputs['user_id'])->first();
            if( $user ){
                $otp = validateOTP($user->email, $inputs['otp']);

                if( $otp->status ) {
                    $password = base64_decode($user->password);
                    $user->otp = null;
                    $user->is_otp_verified = 1;
                    $user->password = bcrypt($password);
                    $user->save();

                    $user = $user->toArray();

                    $user['avtar'] = ( @$user['avtar'] != '' ) ? \Storage::disk('public')->url('images/avtar/'.$user['id'] .'/'.$user['avtar']) : '';

                    $token = (array) getTokenAndRefreshTokenH($user['email'], $password);

                    $address = UserAddress::select('id', 'address', 'latitude', 'longitude')->where('user_id',
                        $user['id'])->first();

                    if ($address) {
                        $address = $address->toArray();
                        $address['address_id'] = $address['id'];
                        unset($address['id']);
                    } else {
                        $address['address_id'] = '';
                        $address['address'] = '';
                        $address['latitude'] = '';
                        $address['longitude'] = '';
                    }
                    $data = array_merge($user, $address);
                    $data = array_merge($data, $token);

                    return response()->json(['data' => $data], 200);
                }else{
                    return response()->json(['error' => ['message' => $otp->message ] ], 422);
//                    return response()->json(['error' => ['message' => 'Invalid otp.please try again' ] ], 422);
                }
            }else{
                return response()->json(['error' => ['message' => 'Incorrect user id' ] ], 422);
            }
        } catch(Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }

    public function login(Request $request){
        try{
            $inputs = $request::all();

            $rules = [
                'email' => 'required',
                'password' => 'required',
            ];

            $validator = Validator::make($request::all(), $rules);
            if ($validator->fails())
                return response()->json(['error' =>  $validator->messages() ], 422);

            $user = User::where('email', $inputs['email'])->first();

            if( !$user ){
                return response()->json(['error' => ['message' => 'Email or password is incorrect'] ], 422);
            }

            if( $user->is_otp_verified ){
                if(Hash::check($inputs['password'], $user->password)) {
                }else{
                    return response()->json(['error' => ['message' => 'Email or password is incorrect'] ], 422);
                }
                $credentials = ['email' => $user->email,'password' => $inputs['password']];
                if (!Auth::attempt($credentials)) {
                    return response()->json(['error' => ['message' => 'Email or password is incorrect']], 422);
                }
                $user = Auth::user()->toArray();

                $user['avtar'] = ( @$user['avtar'] != '' ) ? \Storage::disk('public')->url('images/avtar/'.$user['id'] .'/'.$user['avtar']) : '';
                $token = (array)getTokenAndRefreshTokenH( $user['email'], $inputs['password']);

                $address = UserAddress::select('id', 'address', 'latitude', 'longitude')->where('user_id', $user['id'])->first();

                if( $address ){
                    $address = $address->toArray();
                    $address['address_id'] = $address['id'];
                    unset($address['id']);
                }else{
                    $address['address_id'] = '';
                    $address['address'] = '';
                    $address['latitude'] = '';
                    $address['longitude'] = '';
                }

                $user = array_merge($user, $address);
                $data = array_merge($user, $token);

                return response()->json(['data' => $data], 200);
            }else{
                if( $inputs['password'] != base64_decode($user->password) ){
                    return response()->json(['error' => ['message' => 'Email or password is incorrect'] ], 422);
                }else{
                    $otp = generateOTP($inputs['email'], 4, 2);
                    if( $otp->status ){
                        $user = $user->toarray();
                        $user['otp'] = $otp->token;
                        $res = sendMailH($user, $inputs['email'], 'MailView.registerOtp', 'Verify email for TurtleCare');
                        if( $res == 'success' ){
                            $data['id'] = $user['id'];
                            $data['otp'] = $otp->token;
                            $data['message'] = 'not_verified';
                            return response()->json(['data' => $data], 403);
                        }else{
                            return response()->json(['error' => ['message' => $res ] ], 422);
                        }
                    }else{
                        return response()->json(['error' => ['message' => $otp->message ] ], 422);
                    }
                }
            }
        }catch( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }

    /**
     * Login api
     *
     * @return \Illuminate\Http\Response
     */
    public function getnewtoken(Request $request)
    {
        try {
            $inputs = $request::all();
            $refresh_token = $inputs['refresh_token'];

            if (empty($refresh_token)) {
                return response()->json(['error' => ['message' => "Refresh token required..." ] ], 422);
            }

            $oClient = OClient::where('password_client', 1)->where('provider', 'users')->first();
            $data = [
                'grant_type' => 'refresh_token',
                'refresh_token' => $refresh_token,
                'client_id' => $oClient->id,
                'client_secret' => $oClient->secret,
                'scope' => '*',
            ];
            $response = \Illuminate\Http\Request::create('/oauth/token', 'POST', $data);
            $response = app()->handle($response);
            $data = json_decode($response->getContent());
            if($response->getStatusCode() == 401){
                return response()->json(['error' => ['message' => $data] ], 422);
            }else{
                return response()->json(['data' => $data], 200);
            }
        } catch
        (Exception $e) {
            return response()->json(['error' => ['message' => "Device not found..."] ], 422);
        }
    }

    public function invalidAuth()
    {
        return response()->json(['error' => ['message' => 'Unauthorized' ]], 401);
    }

    public function forgotPassword(Request $request){
        try{
            $inputs = $request::all();

            $rules = [
                'email' => 'required',
            ];

            $validator = Validator::make($request::all(), $rules);
            if ($validator->fails())
                return response()->json(['error' =>  $validator->messages() ], 422);

            $user = User::where('email', $inputs['email'])->first();

            if( $user ){
                $user = $user->toArray();
//                $user['link'] = env('DEEPLINK_HOST') . '/mobile/forgot-password/' . $user['id'];
                $otp = generateOTP($inputs['email'], 4, 2);
                if( $otp->status ) {
                    $user['otp'] = $otp->token;

                    $res = sendMailH($user, $inputs['email'], 'MailView.forgot', 'Forgot password');

                    if ($res == 'success') {
                        $data['id'] = $user['id'];
                        $data['otp'] = $otp->token;
                        $data['message'] = 'Please check your mail';
                        return response()->json(['data' => $data], 200);
                    } else {
                        return response()->json(['error' => ['message' => $res]], 422);
                    }
                }else{
                    return response()->json(['error' => ['message' => $otp->message]], 422);
                }
            }else{
                return response()->json(['error' => ['message' => "Account doesn't exist" ] ], 422);
            }
        }catch( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(Request $request){
        try{
            $inputs = $request::all();

            $rules = [
                'user_id' => 'required',
                'password' => 'required'
            ];

            $validator = Validator::make($request::all(), $rules);
            if ($validator->fails())
                return response()->json(['error' =>  $validator->messages() ], 422);

            $user = User::find($inputs['user_id']);

            if( $user ){
                $bcrypt = bcrypt($inputs['password']);

                $user->password = $bcrypt;
                $user->save();

                return response()->json(['data' => 'Password changed successfully'], 200);
            }else{
                return response()->json(['error' => ['message' => "Account doesn't exist" ] ], 422);
            }
        }catch( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }

    /**
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function resendOtp(Request $request){
        try{
            $inputs = $request::all();
            $rules = [
                'user_id' => 'required',
                'type' => 'required'
            ];
            $validator = Validator::make($request::all(), $rules);
            if ($validator->fails()) {
                return response()->json(['error' =>  $validator->messages() ], 422);
            }

            $user = User::find($inputs['user_id']);
            if( !$user ){
                return response()->json(['error' => ['message' => 'Incorrect user id'] ], 422);
            }

            $otp = generateOTP($user['email'], 4, 2);
            if( $otp->status ){
                $user['otp'] = $otp->token;
                if( $inputs['type'] == 'verify' ){
                    $res = sendMailH($user->toArray(), $user['email'], 'MailView.registerOtp', 'Verify email for TurtleCare');
                }else{
                    $res = sendMailH($user->toArray(), $user['email'], 'MailView.forgot', 'Forgot password');
                }
                if( $res == 'success' ){
                    $data['id'] = $user->id;
                    $data['otp'] = $user['otp'];
                    $data['message'] = 'otp send successfully...';
                    return response()->json(['data' => $data], 200);
                }else{
                    return response()->json(['error' => ['message' => $res ] ], 422);
                }
            }else{
                return response()->json(['error' => ['message' => $otp->message ] ], 422);
            }

        } catch(Exception $e) {
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }
}
