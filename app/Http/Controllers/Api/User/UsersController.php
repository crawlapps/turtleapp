<?php

namespace App\Http\Controllers\Api\User;

use App\Http\Controllers\Controller;
use App\Jobs\AppointmentNotificationJob;
use App\Models\Appointment;
use App\Models\User;
use App\Models\Service;
use App\Models\Staff;
use App\Models\UserAddress;
use App\Traits\ImageTrait;
use App\Traits\PushNotificationTrait;
use Request;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    use PushNotificationTrait;
    public function index(){
        try{
            $staff = User::select('id', 'name', 'type')->get()->toArray();
            return response()->json(['data' => $staff], 200);
        }catch ( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }

    public function getChildPatients(Request $request){
        try{
            $inputs = $request::all();

            $rules = [
                'parent_id' => 'required',
            ];
            $validator = Validator::make($request::all(), $rules);
            if ($validator->fails()) {
                return response()->json(['error' =>  $validator->messages() ], 422);
            }

            $patients = User::with('UserAddress')->select('id', 'firstname', 'lastname', 'email', 'phone', 'parent_relation')->where('parent_id', $inputs['parent_id'])->get();

            $res = [];

            if( count($patients) > 0 ){
                $patients = $patients->toArray();
                foreach ($patients as $key => $value) {
                    $address = getAddressByUser($value['id'],'user');
                    $value = array_merge($value, $address);
                    array_push($res, $value);
                }
            }

            return response()->json(['data' => $res], 200);
        }catch ( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }

    public function editProfile( Request $request ){
        try{
            $inputs = $request::all();

            logger(json_encode($inputs));
            $rules = [
                'user_id' => 'required',
                'email' => 'required|unique:users,email,' . $inputs['user_id'] . ',id',
            ];

            $validator = Validator::make($request::all(), $rules);
            if ($validator->fails()) {
                return response()->json(['error' =>  $validator->messages() ], 422);
            }

            $user = User::find($inputs['user_id']);
            if( $user ){
                if( @$inputs['avtar'] && (gettype(@$inputs['avtar']) != 'string' || @$inputs['avtar'] != null)){

                    if( $user->avtar != '' ){
                        $oldpath = storage_path().'/app/public/images/avtar/'. $inputs['user_id'] .'/'.$user->avtar;
                        if(\File::exists($oldpath)){
                            unlink($oldpath);
                        }
                    }

                    $path = 'images/avtar/' . $inputs['user_id'] . '/';
                    $avtar = ImageTrait::makeImage($inputs['avtar'], $path);
                }else{
                    $avtar = $user->avtar;
                }
                $user->update(array(
                    'user_id' => $inputs['user_id'],
                    'firstname' => $inputs['firstname'],
                    'lastname' => $inputs['lastname'],
                    'email' => $inputs['email'],
                    'phone' => $inputs['phone'],
                    'birthdate' => date('Y-m-d', strtotime($inputs['birthdate'])),
                    'gender' => $inputs['gender'],
                    'avtar' => $avtar,
                ));

                $db_add = UserAddress::where('user_id', $inputs['user_id'])->first();
                $db_add = ( $db_add ) ? $db_add : new UserAddress;
                $db_add->user_id = $inputs['user_id'];
                $db_add->address = (@$inputs['address']) ? $inputs['address'] : '';
                $db_add->latitude = (@$inputs['latitude']) ? $inputs['latitude'] : '';
                $db_add->longitude = (@$inputs['longitude']) ? $inputs['longitude'] : '';
                $db_add->is_main = ($user->is_parent == 1);
                $db_add->save();

                $user = User::where('id', $inputs['user_id'])->first()->toArray();
                $user['avtar'] = ( @$user['avtar'] != '' ) ? \Storage::disk('public')->url('images/avtar/'.$user['id'] .'/'.$user['avtar']) : '';
                $address = UserAddress::select('id', 'address', 'latitude', 'longitude')->where('user_id', $inputs['user_id'])->first();

                if( $address ){
                    $address = $address->toArray();
                    $address['address_id'] = $address['id'];
                    unset($address['id']);
                }else{
                    $address['address_id'] = '';
                    $address['address'] = '';
                    $address['latitude'] = '';
                    $address['longitude'] = '';
                }

                $data = array_merge($user, $address);
                return response()->json(['data' => $data], 200);
            }else{
                return response()->json(['error' =>  ['message' => 'User not found.' ] ], 422);
            }
        }catch ( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }

    public function getAddresses( Request $request ){
        try{
            $inputs = $request::all();

            $rules = [
                'user_id' => 'required',
            ];

            $validator = Validator::make($request::all(), $rules);
            if ($validator->fails()) {
                return response()->json(['error' =>  $validator->messages() ], 422);
            }

            $addresses = UserAddress::select('id', 'address', 'latitude', 'longitude')->where('user_id', $inputs['user_id'])->get();

            return response()->json(['data' => $addresses], 200);
        }catch ( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }

    public function editChildPatient(Request $request){
        try{
            $rules = [
                'user_id' => 'required',
                'child_patient_id' => 'required',
                'relation' => 'required',
            ];

            $validator = Validator::make($request::all(), $rules);
            if ($validator->fails()) {
                return response()->json(['error' =>  $validator->messages() ], 422);
            }
            $inputs = $request::all();

            $user = User::where('id', $inputs['child_patient_id'])->where('parent_id', $inputs['user_id'])->first();
            if( !$user ){
                return response()->json(['error' =>  ['message' => 'User not found.' ] ], 422);
            }

            $user->parent_relation = $inputs['relation'];
            $user->save();

            $data['firstname'] = $user->firstname;
            $data['lastname'] = $user->firstname;
            $data['id'] = $user->id;
            $data['parent_relation'] = $user->parent_relation;
            $address = getAddressByUser($user->id, 'user');
            $res = array_merge($data, $address);
            return response()->json(['data' => $res], 200);
        }catch( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }

    public function deleteChildPatient(Request $request){
        try{
            $rules = [
                'user_id' => 'required',
                'child_patient_id' => 'required',
            ];

            $validator = Validator::make($request::all(), $rules);
            if ($validator->fails()) {
                return response()->json(['error' =>  $validator->messages() ], 422);
            }
            $inputs = $request::all();

            $childUser = User::where('id', $inputs['child_patient_id'])->where('parent_id', $inputs['user_id'])->first();
            if( !$childUser ){
                return response()->json(['error' =>  ['message' => 'User not found.' ] ], 422);
            }

            $childUser->parent_relation = null;
            $childUser->parent_id = null;
            $childUser->is_parent = 1;
            $childUser->refer_by = 0;
            $childUser->save();
            return response()->json(['data' => ['message' =>'Patient deleted successfully.']], 200);
        }catch( \Exception $e ){
            return response()->json(['error' => ['message' => $e->getMessage() ] ], 422);
        }
    }
}
