<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AppointmentNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $appointment;
    private $user_id;
    private $action;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($appointment, $user_id, $action)
    {
        $this->appointment = $appointment;
        $this->user_id = $user_id;
        $this->action = $action;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        createNotificationH($this->appointment, $this->user_id, $this->action);
    }
}
