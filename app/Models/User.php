<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'provider_id',
        'login_type',
        'firstname',
        'lastname',
        'avtar',
        'birthdate',
        'gender',
        'phone',
        'erolled_in_medicadid',
        'use_insurance',
        'insurance_provider',
        'member_number',
        'is_parent',
        'parent_id',
        'parent_relation',
        'referral_code',
        'otp',
        'refer_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function UserAddress(){
        return $this->hasOne(UserAddress::class, 'user_id', 'id')->select('address', 'latitude','longitude');
    }
}
