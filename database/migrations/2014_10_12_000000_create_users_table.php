<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('email')->nullable()->unique();
            $table->string('password')->nullable();
            $table->string('provider_id')->nullable()->comment('Social login id');
            $table->string('login_type')->nullable()->default('custom')->comment('i.e. fb,google,custom...');
            $table->string('firstname')->nullable();
            $table->string('lastname')->nullable();
            $table->string('avtar')->nullable();
            $table->date('birthdate')->nullable();
            $table->char('gender', 1)->nullable();
            $table->string('phone')->nullable();
            $table->boolean('erolled_in_medicadid')->nullable()->comment('1 = yes, 0 = no');
            $table->boolean('use_insurance')->nullable()->comment('1 = yes, 0 = no');
            $table->string('insurance_provider')->nullable();
            $table->string('member_number')->nullable();
            $table->boolean('is_parent')->default(0)->comment('1 = yes, 0 = no');
            $table->unsignedBigInteger('parent_id')->nullable();
            $table->string('parent_relation')->nullable();
            $table->string('referral_code')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
