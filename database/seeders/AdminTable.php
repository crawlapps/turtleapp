<?php

namespace Database\Seeders;

use App\Models\Admin;
use App\Models\Service;
use App\Models\User;
use Illuminate\Database\Seeder;

class AdminTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = ['admin@gmail.com' => '123123'];
        foreach ($admin as $key=>$val){
            User::updateOrcreate([
                'email' => $key,
                'password' => bcrypt($val),
                'user_type' => 'admin',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ], [
                'email' => $key,
                'password' => bcrypt($val),
                'user_type' => 'admin',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]);
        }
    }
}
