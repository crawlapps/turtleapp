<?php

namespace Database\Seeders;

use App\Models\Service;
use App\Models\UserType;
use Illuminate\Database\Seeder;

class ServiceTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $types = ['Covid Test' => 'covid.png', 'Sick or injured' => 'sick-injured.png', 'Annual Physical' => 'annual-physical.png'];
        foreach ($types as $key=>$val){
            Service::updateOrcreate([
                'name' => $key,
                'image' => $val,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ], [
                'name' => $key,
                'image' => $val,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]);
        }
    }
}
