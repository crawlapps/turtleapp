<?php

namespace Database\Seeders;

use App\Models\Staff;
use Illuminate\Database\Seeder;

class StaffTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $staffs = [
            [
                'name' => 'test',
                'type' => 'doctor',
                'specialist_in' => 'xyz'
            ],
            [
                'name' => 'demo',
                'type' => 'nurse',
                'specialist_in' => 'pqr'
            ]
        ];
        foreach ($staffs as $key=>$val){
            Staff::updateOrcreate([
                'name' => $val['name'],
                'type' => $val['type'],
                'specialist_in' => $val['specialist_in'],
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ], [
                'name' => $val['name'],
                'type' => $val['type'],
                'specialist_in' => $val['specialist_in'],
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ]);
        }
    }
}
