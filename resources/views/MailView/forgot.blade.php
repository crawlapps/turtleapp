<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width,minimum-scale=1.0, maximum-scale=1.0" />
</head>
<body>
    <div>
        <h2>Your forgot password link for TurtleCare</h2>
        <p>Hello, {{ $data['firstname'] }} {{ $data['lastname'] }}</p>
        <p>Use below code to reset your password on TurtleCare.</p>
        <p><b>{{ $data['otp'] }}</b></p>
{{--        <p><a class="btn" href="com.turtlecare://www.turtle-app.test/mobile/forgot-password/23" deeplink="true">Reset Password</a></p>--}}
        <p>If you did not request a password reset, just ignore this email and continue using your existing password.</p>
        <p>Thanks,<br>
            The Turtle App Team</p>
    </div>
</body>
</html>
