<html>
<div>
    <h2>Your referral code for TurtleCare</h2>
    <p>Hello, {{ $data['firstname'] }} {{ $data['lastname'] }}</p>
    <p>Welcome to TurtleCare - we're delighted you've joined us! Please register by using the code below.</p>
    <p><b>{{$data['referee_code']}}</b></p>
    <p>Thanks,<br>
        The Turtle App Team</p>
</div>
</html>
