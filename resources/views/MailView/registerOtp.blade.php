<html>
<div>
    <h2>Verify your email for TurtleCare</h2>
    <p>Hello, {{ $data['firstname'] }} {{ $data['lastname'] }}</p>
    <p>Welcome to TurtleCare - we're delighted you've joined us! Please verify by pasting the code below.</p>
    <p><b>{{$data['otp']}}</b></p>
    <p>Thanks,<br>
        The Turtle App Team</p>
</div>
</html>
