<?php

use App\Http\Controllers\Api\Admin\AppointmentController;
use App\Http\Controllers\Api\Service\ServiceController;
use App\Http\Controllers\Api\Staff\StaffController;
use App\Http\Controllers\Api\User\LoginController;
use App\Http\Controllers\Api\User\UsersController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//    Patient api
Route::group(['prefix' => 'user'], function () {
    Route::group([ 'namespace' => 'Api/User'], function() {
        Route::post('register', [LoginController::class, 'register'])->name('register');
        Route::post('verify-otp', [LoginController::class, 'verifyOtp'])->name('verify-otp');
        Route::get('resend-otp', [LoginController::class, 'resendOtp'])->name('resend-otp');
        Route::post('login', [LoginController::class, 'login'])->name('apilogin');
        Route::get('forgot-password', [LoginController::class, 'forgotPassword'])->name('forgotpassword');
        Route::post('change-password', [LoginController::class, 'changePassword'])->name('changepassword');
        Route::get('invalid-auth', [LoginController::class, 'invalidAuth'])->name('invalid-auth');
        Route::post('get-new-token', [LoginController::class, 'getnewtoken'])->name('get-new-token');
    });

    Route::group([ 'middleware' => ['auth:api']], function() {
        Route::get('staff', [StaffController::class, 'index'])->name('staff');
        Route::get('services', [ServiceController::class, 'index'])->name('services');
        Route::post('edit-profile', [UsersController::class, 'editProfile'])->name('edit-profile');
        Route::get('get-child-patients', [UsersController::class, 'getChildPatients'])->name('get-child-patients');
        Route::post('edit-child-patient', [UsersController::class, 'editChildPatient'])->name('edit-child-patient');
        Route::get('delete-child-patient', [UsersController::class, 'deleteChildPatient'])->name('delete-child-patient');
        Route::get('get-addresses', [UsersController::class, 'getAddresses'])->name('get-addresses');


        Route::post('create-appointment', [\App\Http\Controllers\Api\User\AppointmentController::class, 'createAppointment'])->name('create-appointment');
        Route::post('edit-appointment', [\App\Http\Controllers\Api\User\AppointmentController::class, 'createAppointment'])->name('edit-appointment');
        Route::get('get-appointments', [\App\Http\Controllers\Api\User\AppointmentController::class, 'getAppointments'])->name('get-appointments');
        Route::post('get-appointments-by-status', [\App\Http\Controllers\Api\User\AppointmentController::class, 'getAppointmentsByStatus'])->name('get-appointments-by-status');
        Route::post('cancel-appointment', [\App\Http\Controllers\Api\User\AppointmentController::class, 'cancelAppointment'])->name('cancel-appointment');
        Route::post('get-history', [\App\Http\Controllers\Api\User\AppointmentController::class, 'getHistory'])->name('get-history');

    });
});

//    Admin Api
Route::group(['prefix' => 'admin'], function () {
    Route::group([ 'namespace' => 'Api/Admin'], function() {
        Route::post('login', [AppointmentController::class, 'login'])->name('apiadminlogin');
    });

//    Route::group([ 'middleware' => ['auth.admin','auth:api','scopes:admin']], function() {
    Route::group([ 'middleware' => ['auth:api']], function() {
        Route::get('get-staff', [AppointmentController::class, 'getStaff'])->name('get-staff');
        Route::get('get-appointments', [AppointmentController::class, 'getAppointments'])->name('get-appointments');
        Route::post('change-appointment-status', [AppointmentController::class, 'changeAppointmentStatus'])->name('change-appointment-status');
        Route::post('cancel-appointment', [AppointmentController::class, 'cancelAppointment'])->name('cancel-appointment');
    });
});
