<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
/* Deep Link Redirect - For mobile redirect to application */
Route::group(['prefix' => 'mobile'], function() {

    Route::get('forgot-password/{userId}', function($userId) {
        return Redirect::to(env('APP_HOST') . '://forgot-password?userId=' . $userId);
        die();
    });
});
